# Spring Boot Application (Sprint 3 & 4)

### Table of Contents:
1. Recap
- UserRestController
- StockRestController

2. Dockerize
3. AWS Lambda
4. Jenkins & Openshift



### What you managed to get working
- Dockerize both springboot app & mysql
- DockerCompose but dropped it for OpenShift
- Deployment to OpenShift
- CI using Jenkins

### Anything you didn�t quite get round to
- run `mvn` on jenkins because the vm didn't have maven installed

### Who did what
Dockerize - EVERYONE

Deployment w Jenkins & Openshift - Min Han & Redmond

AWS Lambda - Nicole, Rebecca, Min Han