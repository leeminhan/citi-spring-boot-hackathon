package mypackage;

import yahoofinance.Stock;
import yahoofinance.quotes.stock.StockDividend;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Map;

public interface StockService {
    StockWrapper findStock(String ticker);
    BigDecimal findPrice(StockWrapper stock) throws IOException;
    BigDecimal findPerChange(StockWrapper stock) throws IOException;
    StockDividend findDividend(StockWrapper stock) throws IOException;
    Map<String, Stock> getAllStocks(String[] symbols) throws IOException;
}
