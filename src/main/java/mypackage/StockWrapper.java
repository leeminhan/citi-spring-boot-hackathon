package mypackage;

import yahoofinance.Stock;

import java.time.LocalDateTime;

public class StockWrapper {
    private Stock stock;
    private LocalDateTime lastAccessedTime;

    public StockWrapper(Stock stock) {
        this.stock = stock;
        lastAccessedTime = LocalDateTime.now();
    }

    public Stock getStock() {
        return stock;
    }

    public LocalDateTime getLastAccessedTime() {
        return lastAccessedTime;
    }

    @Override
    public String toString() {
        return "StockWrapper{" +
                "stock=" + stock +
                ", lastAccessedTime=" + lastAccessedTime +
                '}';
    }
}
