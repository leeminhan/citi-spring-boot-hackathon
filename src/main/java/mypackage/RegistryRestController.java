package mypackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class RegistryRestController {

    @Autowired
    private UserService service;

    @GetMapping(value = "/user/")
    @ResponseBody
    public ResponseEntity<User> getUserById(@RequestParam int id) {
        User result = service.getUserById(id);
        if (result == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "/user/email/")
    @ResponseBody
    public ResponseEntity<User> getUserByEmail(@RequestParam String email){
        User result = service.getUserByEmail(email);
        if (result == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok().body(result);
    }

    @GetMapping(value = "/user")
    @ResponseBody
    public ResponseEntity<List<User>> getAllUsers(){
        List<User> result = service.getAllUsers();
        if (result == null)
            return ResponseEntity.notFound().build();
        else
            return ResponseEntity.ok().body(result);
    }

    @PostMapping(value = "/user", consumes = {"application/json", "application/xml"}, produces = {"application/json", "application/xml"})
    public User registerUser(@RequestBody User user){
        service.addUser(user);
        return user;
    }

    @PutMapping(value = "/user/",  consumes = {"application/json", "application/xml"})
    @ResponseBody
    public ResponseEntity<String> updateUserById(@RequestParam int id, @RequestBody User user){
        service.updateUserById(id, user);
        return new ResponseEntity<String>("Update by ID Successful", HttpStatus.OK);
    }

    @DeleteMapping(value = "/user/")
    @ResponseBody
    public ResponseEntity<String> deleteUserById(@RequestParam int id){
        service.deleteUserById(id);
        return new ResponseEntity<String>("DELETE by ID Successful", HttpStatus.OK);
    }

    @DeleteMapping(value = "/user/email/")
    @ResponseBody
    public ResponseEntity<String> deleteUserByEmail(@RequestParam String email){
        service.deleteUserByEmail(email);
        return new ResponseEntity<String>("DELETE by EMAIL Successful", HttpStatus.OK);
    }
}
