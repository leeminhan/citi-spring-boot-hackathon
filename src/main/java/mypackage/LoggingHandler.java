package mypackage;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Aspect
@Configuration
public class LoggingHandler {

    @Around("execution(* mypackage.RegistryRestController.*(..))")
    public Object registryAround(ProceedingJoinPoint joinPoint) throws Throwable{
        log.info("=== RegistryRestController: Status = 'EXECUTING' | {} ===", joinPoint.getSignature());
        Object returnValue = joinPoint.proceed();
        log.info("=== RegistryRestController: Status = 'SUCCESSFUL' {} ===", joinPoint.getSignature());
        return returnValue;
    }

    @Around("execution(* mypackage.StockRestController.*(..))")
    public Object stockAround(ProceedingJoinPoint joinPoint) throws Throwable{
        log.info("=== StockRestController: Status = 'EXECUTING' | {} ===", joinPoint.getSignature());
        Object returnValue = joinPoint.proceed();
        log.info("=== StockRestController: Status = 'SUCCESSFUL' {} ===", joinPoint.getSignature());
        return returnValue;
    }
}
