package mypackage;

import org.springframework.stereotype.Service;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;
import yahoofinance.histquotes.HistoricalQuote;
import yahoofinance.quotes.stock.StockDividend;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

@Service
public class StockServiceImpl implements StockService{

    public StockServiceImpl() {
    }

    @Override
    public StockWrapper findStock(String ticker){
        try {
            Stock stock = YahooFinance.get(ticker);
            return new StockWrapper(stock);
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        return null;
    }

    @Override
    public BigDecimal findPrice(StockWrapper stock) throws IOException{
        return stock.getStock().getQuote(true).getPrice();
    }

    @Override
    public BigDecimal findPerChange(StockWrapper stock) throws IOException{
        return stock.getStock().getQuote().getChangeInPercent();
    }

    @Override
    public StockDividend findDividend(StockWrapper stock) throws IOException {
        return stock.getStock().getDividend();
    }

    @Override
    public Map<String, Stock> getAllStocks(String[] symbols) throws IOException{
        try {
            Map<String, Stock> result = YahooFinance.get(symbols);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}

