package mypackage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import javax.sql.DataSource;

@Component
public class SeedDb {

	@Autowired
    JdbcTemplate template;

    @Autowired
    public void SeedDb(DataSource dataSource) {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("insert into user (first_name, last_name, email) values (?, ?, ?)", new Object[]{"Tom", "Daley", "tom_daley@gmail.com"});
        jdbcTemplate.update("insert into user (first_name, last_name, email) values (?, ?, ?)", new Object[]{"Min Han", "lee", "minhan_lee@gmail.com"});
        jdbcTemplate.update("insert into user (first_name, last_name, email) values (?, ?, ?)", new Object[]{"Nicole", "Tan", "nicole_tan@gmail.com"});
        jdbcTemplate.update("insert into user (first_name, last_name, email) values (?, ?, ?)", new Object[]{"Redmond", "G", "redmond_g@gmail.com"});
        jdbcTemplate.update("insert into user (first_name, last_name, email) values (?, ?, ?)", new Object[]{"Rebecca", "Seah", "rebecca_seah@gmail.com"});
    }
}
