package mypackage;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import yahoofinance.Stock;
import yahoofinance.quotes.stock.StockDividend;

import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
public class StockRestController {

    @Autowired
    private StockService stockService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "/stock/", produces={"application/json"})
    public ResponseEntity<StockWrapper> getStock(@RequestParam String ticker, @RequestParam String email){
        User result = userService.getUserByEmail(email);
        if (result != null) {
            StockWrapper stockInfo = stockService.findStock(ticker);
            return ResponseEntity.ok().body(stockInfo);
        }
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping(value = "/stock/{ticker}/price", produces={"application/json"})
    public ResponseEntity<BigDecimal> getPrice(@PathVariable String ticker, @RequestParam String email) throws IOException{
        User result = userService.getUserByEmail(email);
        if (result != null) {
            StockWrapper stockInfo = stockService.findStock(ticker);
            BigDecimal price = stockService.findPrice(stockInfo);
            return ResponseEntity.ok().body(price);
        }
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping (value="/stock/{ticker}/percentageChange", produces={"application/json"})
    public ResponseEntity<BigDecimal> getPerChange (@PathVariable String ticker, @RequestParam String email) throws IOException{
        User result = userService.getUserByEmail(email);
        if (result != null) {
            StockWrapper stockInfo = stockService.findStock(ticker);
            BigDecimal percentageChange = stockService.findPerChange(stockInfo);
            return ResponseEntity.ok().body(percentageChange);
        }
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping (value="/stock/{ticker}/dividend", produces={"application/json"})
    public ResponseEntity<StockDividend> getDividend (@PathVariable String ticker, @RequestParam String email) throws IOException{
        User result = userService.getUserByEmail(email);
        if (result != null) {
            StockWrapper stockInfo = stockService.findStock(ticker);
            StockDividend dividend = stockService.findDividend(stockInfo);
            log.info("dividend: {}", dividend);
            return ResponseEntity.ok().body(dividend);
        }
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping(value = "/stocks", produces={"application/json"})
    public ResponseEntity<Map<String, Stock>> getAllStocks(@RequestParam String email) throws IOException{
        String[] symbols = new String[] {"INTC", "BABA", "AIR.PA"};
        User result = userService.getUserByEmail(email);
        if (result != null) {
            log.info("trace");
            Map<String, Stock> allStocksInfo = stockService.getAllStocks(symbols);
            return ResponseEntity.ok().body(allStocksInfo);
        }
        else
            return ResponseEntity.notFound().build();
    }
}

