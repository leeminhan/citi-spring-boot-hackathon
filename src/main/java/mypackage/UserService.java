package mypackage;

import java.util.List;

public interface UserService {
    User getUserById(int id);
    User getUserByEmail(String email);
    List<User> getAllUsers();
    void addUser(User user);
    void updateUserById(int id, User user);
    void updateUserByEmail(String email, User user);
    void deleteUserById(int id);
    void deleteUserByEmail(String email);
}
